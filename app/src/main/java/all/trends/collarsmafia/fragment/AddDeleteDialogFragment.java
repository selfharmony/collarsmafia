package all.trends.collarsmafia.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import all.trends.collarsmafia.R;


public class AddDeleteDialogFragment extends DialogFragment implements
        DialogInterface.OnClickListener {

    private CirclesDialogFragmentCallback mCirclesDialogFragmentCallback;
    final String LOG_TAG = "myLogs";

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setNeutralButton("ok", this)
                .setNegativeButton("Убрать", this)
                .setPositiveButton("Добавить", this)
                .setMessage("Добавить или убрать жителя");

        return adb.create();

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialog);
        super.onCreate(savedInstanceState);
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case Dialog.BUTTON_NEUTRAL:
                dismiss();
                break;
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        Window window = getDialog().getWindow();

        // set "origin" to top left corner, so to speak
        window.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL);

        // after that, setting values for x and y works "naturally"
//        WindowManager.LayoutParams params = window.getAttributes();
//        params.x = 300;
//        params.y = 100;
//        window.setAttributes(params);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            Button negativeButton = d.getButton(Dialog.BUTTON_NEGATIVE);
            positiveButton.setOnClickListener(v -> mCirclesDialogFragmentCallback.addCircle());
            negativeButton.setOnClickListener(v -> mCirclesDialogFragmentCallback.removeCircle());
        }
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
            Log.d(LOG_TAG, "Dialog 2: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 2: onCancel");
    }

    public void setCirclesDialogFragmentCallback(
            CirclesDialogFragmentCallback circlesDialogFragmentCallback) {
        mCirclesDialogFragmentCallback = circlesDialogFragmentCallback;
    }

    public interface CirclesDialogFragmentCallback {
        void addCircle();

        void removeCircle();

    }

}