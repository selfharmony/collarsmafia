package all.trends.collarsmafia.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import all.trends.collarsmafia.R;


public class MenuListFragment extends Fragment {

    private ImageView ivMenuUserProfilePhoto;
    private MenuNavigationCallback mMenuNavigationCallback;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container,
                false);
        ivMenuUserProfilePhoto = (ImageView) view.findViewById(R.id.ivMenuUserProfilePhoto);
        NavigationView vNavigation = (NavigationView) view.findViewById(R.id.vNavigation);
        vNavigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                String res = String.valueOf(menuItem.getNumericShortcut());
                switch (res) {
                    case "0":
                        mMenuNavigationCallback.newGame();
                        break;
                    case "1":
                        mMenuNavigationCallback.manageCircles();
                        break;
                    case "2":
                        mMenuNavigationCallback.manageSound();
                        break;
                    case "3":
                        mMenuNavigationCallback.showRules();
                        break;
                }

                return false;
            }
        }) ;
//        setupHeader();
        return  view ;
    }

//    private void setupHeader() {
//        int avatarSize = getResources().getDimensionPixelSize(R.dimen.global_menu_avatar_size);
//        String profilePhoto = getResources().getString(R.string.user_profile_photo);
//        Picasso.with(getActivity())
//                .load(profilePhoto)
//                .placeholder(R.drawable.img_circle_placeholder)
//                .resize(avatarSize, avatarSize)
//                .centerCrop()
//                .transform(new CircleTransformation())
//                .into(ivMenuUserProfilePhoto);
//    }

    public void setMenuNavigationCallback(
            MenuNavigationCallback menuNavigationCallback) {
        mMenuNavigationCallback = menuNavigationCallback;
    }

    public interface MenuNavigationCallback {
        void manageCircles();
        void manageSound();
        void newGame();
        void showRules();

    }
}
