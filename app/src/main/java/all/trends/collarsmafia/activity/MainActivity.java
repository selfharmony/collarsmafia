package all.trends.collarsmafia.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;
import com.transitionseverywhere.extra.Scale;

import java.util.ArrayList;

import all.trends.collarsmafia.R;
import all.trends.collarsmafia.fragment.AddDeleteDialogFragment;
import all.trends.collarsmafia.fragment.MenuListFragment;
import all.trends.collarsmafia.utils.CountDownTimerPausable;
import all.trends.collarsmafia.utils.DisplayUtils;
import all.trends.collarsmafia.utils.OnSwipeTouchListener;
import all.trends.collarsmafia.view.ClickConsumerFrameLayout;
import all.trends.collarsmafia.view.UserView;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    private TextView timerTextView;
    private UserView selectedUserView;
    private boolean countdownIsActivated;
    private int timerSeconds;
    private CountDownTimerPausable timer;
    private int mNumViews;
    private ArrayList<UserView> undoArray;
    private ArrayList<UserView> circlesArray;
    private FrameLayout mCirclesHolderLayout;
    private int outerCircleRadiusDp;
    private FrameLayout timerTextFrameLayout;
    private boolean timerTypeIsBig;
    private Handler mHandler;
    private int mDaysCounter;
    private FrameLayout newDayButtonContainer;
    private FlowingDrawer mDrawer;
    private AddDeleteDialogFragment circleChangeDialog;
    private TextView newDayTextView;
    private FrameLayout rulesLayout;
    private boolean beepEnabled = true;
    private boolean rulesAreVisible;
    private ClickConsumerFrameLayout rulesList;
    private boolean usersCleared;
    private boolean doubleBackToExitPressedOnce;

//CD TEST2
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fabric.with(this, new Crashlytics());
        setViews();
        mNumViews = 10;

        setCircles(mCirclesHolderLayout, mNumViews);
    }

    private void setViews() {

        timerTextFrameLayout = (FrameLayout) findViewById(R.id.timerTextView_holder);
        mCirclesHolderLayout = (FrameLayout) findViewById(R.id.circles_holder);
        timerTextView = (TextView) findViewById(R.id.timerTextView);
        newDayButtonContainer = (FrameLayout) findViewById(R.id.new_day_frameLayout);
        mHandler = new Handler();
        mDrawer = (FlowingDrawer) findViewById(R.id.main);
        mDrawer.setTouchMode(ElasticDrawer.TOUCH_MODE_BEZEL);
        circleChangeDialog = new AddDeleteDialogFragment();
        newDayTextView = (TextView) findViewById(R.id.new_day_textView);
        rulesLayout = (FrameLayout) findViewById(R.id.rules);
        rulesList = (ClickConsumerFrameLayout) findViewById(R.id.rules_list);



        setupMenu();
        setupToolbar();
        setTimerSeconds(true);

        setListeners();
    }

    private void setListeners() {
        TransitionSet scaleFadeTransition = new TransitionSet()
                .addTransition(new Scale(0.6f))
                .addTransition(new Fade());

        newDayButtonContainer.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onAnySwipe() {
                if (mDaysCounter > 1) {
                    mDaysCounter--;
                    newDayTextView.setText("День: " + mDaysCounter);
                }
            }

            @Override
            public void onClick() {
                mHandler.postDelayed(() -> {
                    incrementDaysCounter();
                    if (!usersCleared) {
                        refreshUsers();
                    }
                }, 70);
            }
            @Override
            public void onMotionDown() {
                newDayButtonContainer.setAlpha(0.5f);
            }

            @Override
            public void onMotionUp() {
                newDayButtonContainer.setAlpha(1f);
            }

        });



        timerTextFrameLayout.setOnTouchListener(new OnSwipeTouchListener(this) {
            private Runnable timerAlphaRunnable = () -> timerTextView.setAlpha(1f);

            @Override
            public void onAnySwipe() {
                TransitionManager.beginDelayedTransition(timerTextFrameLayout, scaleFadeTransition);
                //                timerTextFrameLayout.setVisibility(timerTypeIsBig ? View
                // .VISIBLE : View.INVISIBLE);
                scaleFadeTransition.setInterpolator(new LinearOutSlowInInterpolator());

                if (timer == null) {
                    timerTextFrameLayout.setVisibility(View.INVISIBLE);
                    mHandler.postDelayed(() -> {
                        timerTypeIsBig = !timerTypeIsBig;
                        setTimerSeconds(timerTypeIsBig);
                        setTimer(timerSeconds, selectedUserView);
                        scaleFadeTransition.setInterpolator(new FastOutLinearInInterpolator());
                        TransitionManager.beginDelayedTransition(timerTextFrameLayout,
                                scaleFadeTransition);
                        timerTextFrameLayout.setVisibility(View.VISIBLE);
                    }, 330);
                }


            }

            @Override
            public void onClick() {

                timerTextView.setAlpha(0.5f);
                mHandler.postDelayed(timerAlphaRunnable, 100);
                if (timer != null) {
                    if (!timer.isPaused()) {
                        timer.pause();
                        if (selectedUserView != null) {
                            selectedUserView.stopValueAnimator(timer.getMillisRemaining());
                        }
                    } else {
                        timer.start();
                        if (selectedUserView != null) {
                            selectedUserView.startValueAnimator();
                        }
                    }
                }

            }
        });


        circleChangeDialog.setCirclesDialogFragmentCallback(
                new AddDeleteDialogFragment.CirclesDialogFragmentCallback() {
                    @Override
                    public void addCircle() {
                        addNewCircle();
                    }

                    @Override
                    public void removeCircle() {
                        removeLastCircle();
                    }

                });

        rulesList.setOnClickListener(v -> showHideRules());
    }

    private void setRulesVisibility(boolean willBeVisible) {
        if (willBeVisible) {
            rulesLayout.setVisibility(View.VISIBLE);
        }else
            rulesLayout.setVisibility(View.GONE);
    }


    private void refreshUsers() {
        usersCleared = true;
        if (timer != null) {
            if (!timer.isPaused()) {
                timer.pause();
            }
            timer.onFinish();
        }
        for (UserView userView : circlesArray) {
            if (userView.getVisibility() == View.VISIBLE) {
                userView.setClicked(false);
                userView.animateSpring();
                userView.setActive(true);
            }
        }

    }

    private void setupMenu() {
        FragmentManager fm = getSupportFragmentManager();
        MenuListFragment mMenuFragment = (MenuListFragment) fm.findFragmentById(
                R.id.id_container_menu);
        if (mMenuFragment == null) {
            mMenuFragment = new MenuListFragment();
            fm.beginTransaction().add(R.id.id_container_menu, mMenuFragment).commit();
        }
        mMenuFragment.setMenuNavigationCallback(
                new MenuListFragment.MenuNavigationCallback() {
                    @Override
                    public void manageCircles() {
                        circleChangeDialog.show(getFragmentManager(), "manageCirclesDialog");
                        mDrawer.closeMenu(true);
                    }

                    @Override
                    public void manageSound() {
                            beepEnabled =! beepEnabled;
                        alertSoundEnabled(beepEnabled);
                        mDrawer.closeMenu(true);
                    }

                    private void alertSoundEnabled(boolean beepEnabled) {
                        if (beepEnabled) {
                            Toast.makeText(getApplicationContext(), "Звук включен",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Звук выключен",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void newGame() {
                        if (timer != null) {
                            if (!timer.isPaused()) {
                                timer.pause();
                            }
                            timer.onFinish();
                        }
                        mCirclesHolderLayout.removeAllViews();
                        mDaysCounter = 1;
                        newDayTextView.setText("День: " + mDaysCounter);
                        setCircles(mCirclesHolderLayout, mNumViews);
                        mDrawer.closeMenu(true);
                    }

                    @Override
                    public void showRules() {
                        showHideRules();
                        mDrawer.closeMenu(true);
                    }
                });
    }

    private void showHideRules() {
        rulesAreVisible = !rulesAreVisible;
        setRulesVisibility(rulesAreVisible);
    }


    protected void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white);

        toolbar.setNavigationOnClickListener(v -> mDrawer.toggleMenu());
    }

    private void setCircles(FrameLayout layout, int numViews) {
        mDaysCounter = 1;
        circlesArray = new ArrayList<>();
        undoArray = new ArrayList<>();
        outerCircleRadiusDp = DisplayUtils.convertDpToPixel(this, 133);
        int width = DisplayUtils.getDisplayWidth(this);
        int koef = numViews / 2;
        int circleRadius = width / koef;
        for (int i = 0; i < numViews; i++) {
            // Create some quick TextViews that can be placed.
            UserView v = new UserView(this);
            circlesArray.add(v);
            v.setOnTouchListener(new OnSwipeTouchListener(this) {


                @Override
                public void onMotionDown() {
                    v.pushSpring();
                }

                @Override
                public void onMotionUp() {
                    v.returnSpring();
                }

                @Override
                public void onClick() {

                    if (!countdownIsActivated && !v.isClicked()) {
                        countdownIsActivated = true;
//                        v.setClicked(true);
                        if (usersCleared)
                            usersCleared = false;
                        setTimer(timerSeconds, v);
                        selectedUserView = v;
                        v.getIconImageView().setImageResource(R.color.colorCircleTransparent);
                        timer.start();
                    } else if (countdownIsActivated && v.equals(selectedUserView)){
                        v.returnSpring();
                        timerSkip();
                    } else
                        v.returnSpring();
                }

                private void timerSkip() {
                    if (timer != null) {
                        if (!timer.isPaused()) {
                            timer.pause();
                        }
                        timer.onFinish();
                    }
                }

                @Override
                public void onAnySwipe() {
                    removeLastCircle(v);
                }

            });


            // Set a text and center it in each view.
//            v.setText("View " + i);
//            v.setGravity(Gravity.CENTER);
            v.setActive(true);
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(circleRadius, circleRadius);
            // Place all views in the center of the layout. We'll transform them
            // away from there in the code below.
            lp.gravity = Gravity.CENTER;
            // Set layout params on view.
            v.setLayoutParams(lp);
            // Calculate the angle of the current view. Adjust by 90 degrees to
            // get View 0 at the top. We need the angle in degrees and radians.
            float angleDeg = i * 360.0f / numViews - 90.0f;
            float angleRad = (float) (angleDeg * Math.PI / 180.0f);
            // Calculate the position of the view, offset from center (300 px from
            // center). Again, this should be done in a display size independent way.
//            v.setX(400*(float) Math.cos(angleRad));
//            v.setY(400*(float) Math.sin(angleRad));

            v.setTranslationX(outerCircleRadiusDp * (float) Math.cos(angleRad));
            v.setTranslationY(outerCircleRadiusDp * (float) Math.sin(angleRad));
            // Set the rotation of the view.
//            v.setRotation(angleDeg + 90.0f);
            v.getUserTextView().setText(String.valueOf(i + 1));
//            v.getUserTextView().setRotation(270f - angleDeg);
            layout.addView(v);
            v.invalidate();
            v.animateSpringShort();
        }
    }

    private void removeLastCircle(UserView v) {
        if (!countdownIsActivated) {
            undoArray.add(v);
            v.setVisibility(View.GONE);
        }
    }

    private void setTimerSeconds(Boolean isTimerBig) {
        timerTypeIsBig = isTimerBig;
        if (isTimerBig) {
            timerSeconds = 60;

        } else {
            timerSeconds = 30;
        }
        timerTextView.setText(String.valueOf(timerSeconds));
    }

    private void setTimer(int timerSeconds, final View v) {
        long millis = timerSeconds * 1000;
        if (v != null) {
            ((UserView) v).shrinkSpringOverTime((int) millis);
            timer = new CountDownTimerPausable(millis, 1000, beepEnabled, timerTextView) {
                @Override
                public void onTick(long millisUntilFinished) {
                    timerTextView.setText(String.valueOf(millisUntilFinished / 1000));
                }

                @Override
                public void onFinish() {
                    if (!((UserView) v).isClicked()) {
                        ((UserView) v).setActive(false);
                        countdownIsActivated = false;
                        timerTextView.setText("ok");
                        mHandler.postDelayed(
                                () -> timerTextView.setText(String.valueOf(timerSeconds)), 700);
                        ((UserView) v).stopValueAnimator(millis);
                        ((UserView) v).animateSpring();
                        timer = null;
                        selectedUserView = null;
                    }

                }
            };
        }
    }

    @Override
    public void onBackPressed() {
        if (!undoArray.isEmpty()) {
            UserView userView = undoArray.get(undoArray.size() - 1);
            userView.setVisibility(View.VISIBLE);
            userView.animateSpring();
            undoArray.remove(undoArray.size() - 1);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Кликните второй раз чтобы выйти", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(() -> doubleBackToExitPressedOnce =false, 2000);
        }
    }

    private int incrementNumViews(int numViews) {
        if (numViews < 15) {
            numViews++;
        }
        return numViews;
    }

    private int decrementNumViews(int numViews) {
        if (numViews > 8) {
            numViews--;
        }
        return numViews;
    }

    private void removeLastCircle() {
        if (timer != null) {
            if (!timer.isPaused()) {
                timer.pause();
            }
            timer.onFinish();
        }
        mCirclesHolderLayout.removeAllViews();
        mNumViews = decrementNumViews(mNumViews);
        setCircles(mCirclesHolderLayout, mNumViews);
        newDayTextView.setText("День: " + mDaysCounter);
    }

    private void addNewCircle() {
        if (timer != null) {
            if (!timer.isPaused()) {
                timer.pause();
            }
            timer.onFinish();
        }
        mCirclesHolderLayout.removeAllViews();
        mNumViews = incrementNumViews(mNumViews);
        setCircles(mCirclesHolderLayout, mNumViews);
        newDayTextView.setText("День: " + mDaysCounter);
    }


    public void incrementDaysCounter() {
        mDaysCounter++;
        newDayTextView.setText("День: " + mDaysCounter);
    }
}
