package all.trends.collarsmafia.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;

import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringSystem;
import com.facebook.rebound.SpringUtil;

import all.trends.collarsmafia.R;
import de.hdodenhof.circleimageview.CircleImageView;

//Created by selfharmony
public class UserView extends FrameLayout {
    private Spring mScaleSpring;
    private SpringSystem springSystem = SpringSystem.create();
    private CircleImageView mIconImageView;
    private Handler handler;
    private FontedTextView mUserTextView;

    private float dX;
    private float dY;
    private Float initialLeft;
    private Float initialTop;
    private FrameLayout.LayoutParams viewLayoutParams;
    private boolean mClicked;
    private ValueAnimator mValueAnimator;


    public UserView(Context context) {
        super(context);
        createView();
    }

    public UserView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView();
    }

    public UserView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public UserView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView();
    }


    private void initSpringAnimation() {
        mScaleSpring = springSystem.createSpring();
        SpringConfig springConfig = SpringConfig.fromOrigamiTensionAndFriction(100, 4);
        mScaleSpring.setSpringConfig(springConfig);

        // Add a listener to observe the motion of the mScaleSpring.
        mScaleSpring.addListener(new SimpleSpringListener() {
            @Override
            public void onSpringUpdate(Spring spring) {
                float mappedValue = (float) SpringUtil.mapValueFromRangeToRange(
                        spring.getCurrentValue(), 0, 1, 1, 0.64);
                setScaleY(mappedValue);
                setScaleX(mappedValue);
            }
        });
        mScaleSpring.setCurrentValue(0);
    }

    private void createView() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View view = layoutInflater.inflate(R.layout.user_view, this, false);
        mIconImageView = (CircleImageView) view.findViewById(R.id.circle_icon_view);
        mUserTextView = (FontedTextView) view.findViewById(R.id.user_text);
//        mIconImageView.setImageResource(R.color.colorAccent);
        addView(view);
        handler = new Handler();
        initSpringAnimation();

    }


    public CircleImageView getIconImageView() {
        return mIconImageView;
    }

    public FontedTextView getUserTextView() {
        return mUserTextView;
    }

    public void pushSpring() {
        mScaleSpring.setEndValue(1);
    }

    public void returnSpring() {
        mScaleSpring.setEndValue(0);
    }

    public void animateSpring() {
        mScaleSpring.setEndValue(1);
        handler.postDelayed(() -> mScaleSpring.setEndValue(0), 105);
    }

    public void animateSpringShort() {
        mScaleSpring.setEndValue(1);
        handler.postDelayed(() -> mScaleSpring.setEndValue(0), 35);
    }

    public void shrinkSpringOverTime(int timerSeconds) {
        mValueAnimator = ValueAnimator.ofFloat(0, 1);
        mValueAnimator.setInterpolator(new DecelerateInterpolator());
        mValueAnimator.setDuration(timerSeconds);
        mValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                mScaleSpring.setEndValue(value);
                invalidate();
            }
        });
        mValueAnimator.start();

    }

    public boolean isClicked() {
        return mClicked;
    }

    public void setActive(boolean active) {
        mClicked = !active;
        if (active) {
//            mUserTextView.setAlpha(1f);
            getIconImageView().setBorderWidth(2);
            getIconImageView().setBorderColor(Color.WHITE);
            getIconImageView().setAlpha(1f);
            getIconImageView().setImageResource(R.color.colorBackground);
        } else {
            getIconImageView().setImageResource(R.drawable.the_collar_var_1);
            getIconImageView().setAlpha(0.7f);
//            mUserTextView.setAlpha(0f);
        }
    }

    public void setClicked(boolean clicked) {
        mClicked = clicked;
    }

    //    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        setViewTouchEvent(event);
//        return true;
//    }
//
//    private void setViewTouchEvent(MotionEvent event) {
//        switch (event.getActionMasked()) {
//            case MotionEvent.ACTION_DOWN:
//
//                dX = event.getX();
//                dY = event.getY();
//
//            case MotionEvent.ACTION_MOVE:
//                setX(Math.round(event.getX()));
//                setY(Math.round((event.getY())));
////                int viewXPosition = Math.round(event.getX() - dX);
////                int viewYPosition = Math.round(event.getY() - dY);
////                setPosition(viewXPosition, viewYPosition);
//
//            case MotionEvent.ACTION_UP:
//                this.animate()
//                        .x(dX)
//                        .y(dY)
//                        .setDuration(200)
//                        .start();
//        }
//    }

    public void stopValueAnimator(long millisRemaining) {
        float value = (float) mValueAnimator.getAnimatedValue();
        mValueAnimator.cancel();
        mValueAnimator.setFloatValues(value, 1);
        mValueAnimator.setDuration(millisRemaining);
    }


    public void startValueAnimator() {
        mValueAnimator.start();
    }
}
