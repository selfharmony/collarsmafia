package all.trends.collarsmafia.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

//Created by selfharmony
public class ClickConsumerFrameLayout extends FrameLayout {
    public ClickConsumerFrameLayout(Context context) {
        super(context);
    }

    public ClickConsumerFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClickConsumerFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ClickConsumerFrameLayout(Context context, AttributeSet attrs, int defStyleAttr,
            int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

        @Override
        public boolean dispatchTouchEvent(MotionEvent e) {
            // do what you need to with the event, and then...
            return super.dispatchTouchEvent(e);
        }
}
