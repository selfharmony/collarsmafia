package all.trends.collarsmafia.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * TODO: document your custom view class.
 */
public class FontedTextView extends TextView {

    public FontedTextView(Context context) {
        super(context);
        setFont();
    }
    public FontedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }
    public FontedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont();
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public FontedTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setFont();
    }

    private void setFont() {
        setTextColor(Color.WHITE);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Furore.otf");
        setTypeface(tf);
    }

}
