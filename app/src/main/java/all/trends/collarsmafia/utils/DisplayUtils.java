package all.trends.collarsmafia.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

import all.trends.collarsmafia.App;

/**
 * Created by Roman Choriew .
 */

public abstract class DisplayUtils {
    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into
     *           pixels
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static int convertDpToPixel(float dp) {
        Resources resources = App.getContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

    public static int convertDpToPixel(Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

    public static int getDisplayHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static int getDisplayWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px) {
        Resources resources = App.getContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static int getViewXPosition(View view) {
        int[] positionArray = new int[2];
        view.getLocationInWindow(positionArray);
        return positionArray[0];
    }

    public static int getViewYPosition(View view) {
        int[] positionArray = new int[2];
        view.getLocationInWindow(positionArray);
        return positionArray[1];
    }

    public static boolean viewInTouchArea(View view, MotionEvent motionEvent) {
        int viewPositionX = DisplayUtils.getViewXPosition(view);
        int viewPositionY = DisplayUtils.getViewYPosition(view);
        int touchX = (int) motionEvent.getX();
        int touchY = (int) motionEvent.getY();
        int viewHeight = view.getHeight();
        int viewWidth = view.getWidth();

        return touchX > viewPositionX && touchX < (viewPositionX + viewWidth)
                && touchY > viewPositionY && touchY < (viewPositionY + viewHeight);
    }

    public static boolean viewInViewArea(View staticView, View draggableView) {
        int firstViewPositionX = DisplayUtils.getViewXPosition(staticView);
        int firstViewPositionY = DisplayUtils.getViewYPosition(staticView);
        int firstViewHeight = staticView.getHeight();
        int firstViewWidth = staticView.getWidth();
        return isViewInBounds(draggableView, firstViewPositionX, firstViewPositionY) || isViewInBounds(
                draggableView, firstViewPositionX, firstViewPositionY + firstViewHeight)
                || isViewInBounds(draggableView, firstViewPositionX + firstViewWidth,
                firstViewPositionY) || isViewInBounds(draggableView,
                firstViewPositionX + firstViewWidth, firstViewPositionY + firstViewHeight);
    }

    public static boolean isViewInBounds(View view, int x, int y) {
        Rect outRect = new Rect();
        int[] location = new int[2];

        view.getDrawingRect(outRect);
        view.getLocationOnScreen(location);
        outRect.offset(location[0], location[1]);
        return outRect.contains(x, y);
    }


    @SuppressLint("NewApi")
    public static int getSoftButtonsBarHeight(Activity activity) {
        // getRealMetrics is only available with API 17 and +
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            activity.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;
    }

    @SuppressLint("NewApi")
    public static int getRealHeight(Activity activity) {
        // getRealMetrics is only available with API 17 and +
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            return metrics.heightPixels;
        }
        return 0;
    }
}
