package all.trends.collarsmafia.utils;

import android.graphics.Color;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import all.trends.collarsmafia.App;
import all.trends.collarsmafia.R;

public abstract class CountDownTimerPausable {
    private long millisInFuture = 0;
    private long countDownInterval = 0;
    private long millisRemaining =  0;

    private CountDownTimer countDownTimer = null;
    private ToneGenerator toneG;
    private boolean isPaused = true;
    private boolean beepEnabled;
    private TextView mTimerTextView;
    private boolean textColorIsSet;

    public CountDownTimerPausable(long millisInFuture, long countDownInterval, boolean isBeep,
            TextView timerTextView) {
        super();
        toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
        beepEnabled = isBeep;
        mTimerTextView = timerTextView;

        this.millisInFuture = millisInFuture;
        this.countDownInterval = countDownInterval;
        this.millisRemaining = this.millisInFuture;

        mTimerTextView.setTextColor(Color.WHITE);
    }
    private void createCountDownTimer(){
        countDownTimer = new CountDownTimer(millisRemaining,countDownInterval) {

            @Override
            public void onTick(long millisUntilFinished) {
                millisRemaining = millisUntilFinished;
                CountDownTimerPausable.this.onTick(millisUntilFinished);

                if (millisRemaining < 11000) {
                    mTimerTextView.setTextColor(ContextCompat.getColor(App.getContext(), R.color.colorPrimaryDark));
                    textColorIsSet = true;
                }
                if (beepEnabled) {
                    if (millisRemaining < 11000 && millisRemaining > 10000) {
                        toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 250);
                    }
                    if (millisRemaining < 4000) {
                        toneG.startTone(ToneGenerator.TONE_PROP_BEEP2, 200);
                    }
                }
            }

            @Override
            public void onFinish() {
                    mTimerTextView.setTextColor(Color.WHITE);
                    if (beepEnabled)
                        toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD,  750);
                CountDownTimerPausable.this.onFinish();
            }
        };
    }
    /**
     * Callback fired on regular interval.
     *
     * @param millisUntilFinished The amount of time until finished.
     */
    public abstract void onTick(long millisUntilFinished);
    /**
     * Callback fired when the time is up.
     */
    public abstract void onFinish();
    /**
     * Cancel the countdown.
     */
    public final void cancel(){
        if(countDownTimer!=null){
            countDownTimer.cancel();
        }
        this.millisRemaining = 0;
    }
    /**
     * Start or Resume the countdown.
     * @return CountDownTimerPausable current instance
     */
    public synchronized final CountDownTimerPausable start(){
        if(isPaused){
            createCountDownTimer();
            countDownTimer.start();
            isPaused = false;
        }
        return this;
    }
    /**
     * Pauses the CountDownTimerPausable, so it could be resumed(start)
     * later from the same point where it was paused.
     */
    public void pause()throws IllegalStateException{
        if(!isPaused){
            mTimerTextView.setTextColor(Color.WHITE);
            countDownTimer.cancel();
        } else{
            throw new IllegalStateException("CountDownTimerPausable is already in pause state, start counter before pausing it.");
        }
        isPaused = true;
    }
    public boolean isPaused() {
        return isPaused;
    }

    public long getMillisRemaining() {
        return millisRemaining;
    }

    public void setMillisRemaining(long millisRemaining) {
        this.millisRemaining = millisRemaining;
    }

}