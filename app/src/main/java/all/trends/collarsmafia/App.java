package all.trends.collarsmafia;

import android.app.Application;
import android.content.Context;

/**
 * Created by Roman Choriew .
 */

public class App extends Application {

    private static Context context;

    public App() {
        context = this;
    }
    public static Context getContext() {
        return context;
    }

}
